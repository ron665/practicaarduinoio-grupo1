#include <iostream>
#include <string>
#include <unistd.h>
#include <math.h>
#include "arduino-serial-lib.h"


using namespace std;

struct node_data{
	float temperature;
	float humidity;
	node_data* next;
};

struct list_data{
	node_data* front;
};

void init_list(list_data* _list){
	_list->front = NULL;
}

void add_node_temperature(list_data* _list, node_data* _node){
	if(_list->front == NULL){
		_list->front = _node;
		_node->next = NULL;
		return;
	}
	if(_list->front->temperature > _node->temperature){
		_node->next = _list->front;
		_list->front = _node;
		return;
	}
	node_data* it = _list->front;
	while(it != NULL){
		if(it->next != NULL && (it->next->temperature > _node->temperature)){
			_node->next = it->next;
			it->next = _node;
			return;
		}
		if(it->next == NULL){
			it->next = _node;
			_node->next = NULL;
			return;
		}
		it = it->next;
	}
}

void add_node_humidity(list_data* _list, node_data* _node){
	if(_list->front == NULL){
		_list->front = _node;
		_node->next = NULL;
		return;
	}
	if(_list->front->humidity > _node->humidity){
		_node->next = _list->front;
		_list->front = _node;
		return;
	}
	node_data* it = _list->front;
	while(it != NULL){
		if(it->next != NULL && (it->next->humidity > _node->humidity)){
			_node->next = it->next;
			it->next = _node;
			return;
		}
		if(it->next == NULL){
			it->next = _node;
			_node->next = NULL;
			return;
		}
		it = it->next;
	}
}

void add_node(list_data* _list, node_data* _node){
	if(_list->front == NULL){
		_list->front = _node;
		_node->next = NULL;
		return;
	}

	node_data* it = _list->front;
	while(it != NULL){
		if(it->next != NULL){
			_node->next = it->next;
			it->next = _node;
			return;
		}
		if(it->next == NULL){
			it->next = _node;
			_node->next = NULL;
			return;
		}
		it = it->next;
	}
}

void print_all_data(list_data* _list){
	int count = 0;
	node_data* it = _list->front;
	while(it != NULL){
		count++;
		cout << "Lectura #" << count << ", temperatura: " << it->temperature << ", humedad: " << it->humidity << endl;
		it = it->next;
	}
}

void print_min_tmp(list_data* _list){	
	cout << "Minimo de temperatura: " << _list->front->temperature << endl;
}

void print_min_hum(list_data* _list){	
	cout << "Minimo de humedad: " << _list->front->humidity << endl;
}

void print_max_tmp(list_data* _list){	
	node_data* it = _list->front;
	node_data* max;
	while(it != NULL){
		if(it->next == NULL){
			max = it;
		}
		it = it->next;
	}
	cout << "Maximo de temperatura: " << max->temperature << endl;
}

void print_max_hum(list_data* _list){	
	node_data* it = _list->front;
	node_data* max;
	while(it != NULL){
		if(it->next == NULL){
			max = it;
		}
		it = it->next;
	}
	cout << "Maximo de humedad: " << max->humidity << endl;
}

void print_med_hum(list_data* _list){	
	float avg = 0.0;
	int count = 0;
	node_data* it = _list->front;
	node_data* med;
	while(it != NULL){
		count++;
		avg += it->humidity;
		it = it->next;
	}
	avg = avg / count;
	cout << "Media de humedad: " << avg << endl;
}

void print_med_tmp(list_data* _list){	
	float avg = 0.0;
	int count = 0;
	node_data* it = _list->front;
	node_data* med;
	while(it != NULL){
		count++;
		avg += it->temperature;
		it = it->next;
	}
	avg = avg / count;
	cout << "Media de temperatura: " << avg << endl;
}

void print_std_hum(list_data* _list){

	float avg = 0.0;
	int count = 0;
	float standardDeviation = 0.0;
	node_data* it = _list->front;
	node_data* med;
	while(it != NULL){
		count++;
		avg += it->humidity;
		it = it->next;
	}
	avg = avg / count;

	node_data* it_2 = _list->front;
	while(it_2 != NULL)
        standardDeviation += pow(it_2->humidity - avg, 2);

    cout << "Desviacion estandard de la humedad: " << sqrt(standardDeviation / count) << endl;
}

void print_std_tmp(list_data* _list){

	float avg = 0.0;
	int count = 0;
	float standardDeviation = 0.0;
	node_data* it = _list->front;
	node_data* med;
	while(it != NULL){
		count++;
		avg += it->temperature;
		it = it->next;
	}
	avg = avg / count;

	node_data* it_2 = _list->front;
	while(it_2 != NULL)
        standardDeviation += pow(it_2->temperature - avg, 2);

    cout << "Desviacion estandard de la humedad: " << sqrt(standardDeviation / count) << endl;
}


int main( int argc, char** argv )
{  
	list_data list_order_by_temperature;
	list_data list_order_by_humidity;
	list_data all_data_as_arrives;
	init_list(&list_order_by_humidity);
	init_list(&list_order_by_temperature);
	init_list(&all_data_as_arrives);

	int times;
	do{
		do{

			node_data* node = new node_data;


			const int buf_max = 256;

			int fd = -1;
		    char serialport[buf_max];
		    int baudrate = 9600;  // default
		    char quiet=0;
		    char eolchar = '\n';
		    int timeout = 1000;
		    char buf[buf_max];
			int rc,n;

			fd = serialport_init("/dev/ttyACM0", baudrate);

			serialport_flush(fd);
			if( fd == -1 ) printf("serial port not opened");
		        sprintf(buf, "%s", argv[1]);

		        printf("send string:%s\n", buf);
		        rc = serialport_write(fd, "t");

			if(rc==-1) printf("error writing");

			if( fd == -1 ) printf("serial port not opened\n");
	        memset(buf,0,buf_max);  //
	        serialport_read_until(fd, buf, eolchar, buf_max, timeout);
	        printf("read string:");

	        int d = convertBinaryToDecimal(buf);

			node->temperature = (float)d;

			serialport_flush(fd);
			if( fd == -1 ) printf("serial port not opened");
		        sprintf(buf, "%s", argv[1]);

		        printf("send string:%s\n", buf);
		        rc = serialport_write(fd, "h");

			if(rc==-1) printf("error writing");

			if( fd == -1 ) printf("serial port not opened\n");
	        memset(buf,0,buf_max);  //
	        serialport_read_until(fd, buf, eolchar, buf_max, timeout);
	        printf("read string:");

	        int d = convertBinaryToDecimal(buf);

			node->humidity = (float)d;

			add_node(&all_data_as_arrives, node);
			add_node_humidity(&list_order_by_humidity, node);
			add_node_temperature(&list_order_by_temperature, node);

			print_all_data(&all_data_as_arrives);
			print_min_hum(&list_order_by_humidity);
			print_max_hum(&list_order_by_humidity);
			print_med_hum(&list_order_by_humidity);
			print_std_hum(&list_order_by_humidity);
			print_min_tmp(&list_order_by_temperature);
			print_max_tmp(&list_order_by_temperature);
			print_med_tmp(&list_order_by_temperature);
			print_std_tmp(&list_order_by_temperature);
			sleep(60);
			times++;
		}while(times < 5);
		times = 0;
		init_list(&list_order_by_humidity);
		init_list(&list_order_by_temperature);
		init_list(&all_data_as_arrives);
	}while(true);

	return 0;
}