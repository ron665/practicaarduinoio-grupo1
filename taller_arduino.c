#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arduino-serial-lib.h"

int convertBinaryToDecimal(long long n)
{
    int decimalNumber = 0, i = 0, remainder;
    while (n!=0)
    {
        remainder = n%10;
        n /= 10;
        decimalNumber += remainder*pow(2,i);
        ++i;
    }
    return decimalNumber;
}

int main(int argc, char const *argv[])
{
	const int buf_max = 256;

	int fd = -1;
    char serialport[buf_max];
    int baudrate = 9600;  // default
    char quiet=0;
    char eolchar = '\n';
    int timeout = 1000;
    char buf[buf_max];
	int rc,n;

	fd = serialport_init("/dev/ttyACM0", baudrate);

	serialport_flush(fd);
	if( fd == -1 ) printf("serial port not opened");
        sprintf(buf, "%s", argv[1]);

        printf("send string:%s\n", buf);
        rc = serialport_write(fd, buf);

	if(rc==-1) printf("error writing");

	if( fd == -1 ) printf("serial port not opened\n");
        memset(buf,0,buf_max);  //
        serialport_read_until(fd, buf, eolchar, buf_max, timeout);
        printf("read string:");

        int d = convertBinaryToDecimal(buf);

		printf("%d\n", d);	
}

